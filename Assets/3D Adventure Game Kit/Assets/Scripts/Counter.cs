﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// A basic counter script can be used for multiple purposes however is used display the amount of coins that have been collected.
/// </summary>
public class Counter : MonoBehaviour {
    int value = 0;  //Int on the current count of the counter.
    Text text;      //Visual display of the counter.
	public Stairs stair;
	public AudioClip teleportSfx;   //Sound Effect used on teleport.
	public GameObject directLight;		// turn off direct light after reaching 7 coins

    /// <summary>
    /// Finds the Text component on the same GameObject.
    /// </summary>
    void Start() {
        text = GetComponent<Text>();
    }

    /// <summary>
    /// Is called when the value is increased and is show on the stored text variable.
    /// </summary>
    public void Increase() {
        value++;

		if (value >= 7) {
			SoundManager.instance.Play(teleportSfx);
			if (stair) {
				stair.setVisibility (true);
			} else {
				Debug.Log ("Didn't link stair to coin counter text ui.");
			}

			if (directLight) {
				directLight.SetActive (false);
			} else {
				Debug.Log ("Didn't link direct light to coin counter text ui.");
			}
		}

        text.text =  value.ToString() + " / 7";
    }
}
