﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Blazier : MonoBehaviour {
	GameObject fire;
	public Text WinText;
	public AudioClip sfx;   //Sound Effect used on victory.

	void Start () {
		fire = this.gameObject.transform.GetChild (0).gameObject;
		fire.SetActive (false);
	}

	void OnTriggerEnter(Collider c) {
		if (c.tag == "Player") {
			fire.SetActive (true);
			WinText.text = "You Win!";

			if (sfx != null) {
				SoundManager.instance.Play(sfx);
			}
		}
	}
}

