﻿using UnityEngine;
using System.Collections;

public class ChurchGate : MonoBehaviour {
	bool openGate = false;
	bool closeGate = false;
	bool gateOpened = false;
	public AudioClip doorUpSfx;   //Sound Effect used on door sliding up.
	public AudioClip doorDownSfx;   //Sound Effect used on door sliding down.
	float distance = 4.0f;

	// Use this for initialization
	void Start () {
		openGate = false;
		closeGate = false;
		gateOpened = false;
		distance = 4.0f;
	}
	
	// Update is called once per frame
	void Update () {
		float offset = Time.fixedDeltaTime * 2;

		if (openGate) {
			if (distance - offset > 0) {
				transform.Translate (Vector3.up * offset);
				distance -= Vector3.up.y * offset;
			} else if (distance > 0 && distance  <= offset) {
				transform.Translate (Vector3.up * distance);
				distance = 0f;
				SoundManager.instance.efxSource.Stop ();
				openGate = false;
				gateOpened = true;
			}
		}

		if (closeGate) {
			if (distance + offset < 4) {
				transform.Translate (Vector3.down * offset);
				distance += Vector3.up.y * offset;
			} else if (distance < 4 && distance + offset >= 4) {
				transform.Translate (Vector3.down * (4 - distance));
				distance = 4f;
				SoundManager.instance.efxSource.Stop ();
				closeGate = false;
				gateOpened = false;
			}
		}
	}

	void OnTriggerEnter(Collider c) {
		if (c.tag == "Player" && gateOpened == false) {
			openGate = true;
			SoundManager.instance.Play(doorUpSfx);
		}
	}

	void OnTriggerExit (Collider c) {
		if (c.tag == "Player" && gateOpened == true) {
			closeGate = true;
			SoundManager.instance.Play(doorDownSfx);
		}
	}
}

