using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectibleManager : MonoBehaviour {
    public GameObject collectiblePrefab;                // The collectible prefab to be spawned.
    public int spawnNumbers = 5;
    public List<Transform>spawnPoints = new List<Transform>();         // An array of the spawn points that collectibles can spawn from.
    private List<GameObject> collectibles = new List<GameObject>();       // array that stores the collectibles instantiated
    private List<bool> isInstantiated = new List<bool>();

    void Start () {
        for (int i = 0; i < spawnPoints.Count; i++) {
            isInstantiated.Add (false);
        }
       
        Spawn ();
        // Check how many collectibles are instantiated and inside the list
        if (collectibles.Count == 0) {
            Debug.Log ("Fail to instantiate any collectible.");
            return;
        } else if (collectibles.Count < spawnNumbers) {
            Debug.Log ("Instantiated " + collectibles.Count + "collectibles." );
        }
    }

    void Spawn () {
        if(spawnNumbers > spawnPoints.Count) {
            Debug.Log ("Spawn number is larger than spawn point number.");
            return;
        }

        for (int i = 0; i < spawnNumbers; i++) {
             // Find a random index between zero and one less than the number of spawn points.
            int spawnPointIndex = Random.Range (0, spawnPoints.Count);
            // check if that index has already been spawn
            while (isInstantiated[spawnPointIndex]) {
                spawnPointIndex = Random.Range (0, spawnPoints.Count);
            }
            isInstantiated[spawnPointIndex] = true;

            // Create an instance of the collectible prefab at the randomly selected spawn point's position and rotation.
            GameObject newCollectible = Instantiate (collectiblePrefab, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation) as GameObject;
            // push the instantiated Collectible into the array list if it exists
            if (newCollectible != null) {
                collectibles.Add (newCollectible);
            } else {
                Debug.Log ("Fail to instantiate the collectible.");
                continue;
            }
        }        
    }
}