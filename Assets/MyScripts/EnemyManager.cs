using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour {
    public GameObject enemyPrefab;                // The enemy prefab to be spawned.
    public int spawnNumbers = 18;
    public List<Transform>spawnPoints = new List<Transform>();         // An array of the spawn points this enemy can spawn from.    
    private List<GameObject> enemies = new List<GameObject>();       // array that stores the enemies instantiated
    private List<bool> isInstantiated = new List<bool>();

    void Start () {
        for (int i = 0; i < spawnPoints.Count; i++) {
            isInstantiated.Add (false);
        }
        // InvokeRepeating ("Spawn", spawnTime, spawnTime);        // Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
        Spawn ();
        // Check how many enemies are instantiated and inside the list
        if (enemies.Count == 0) {
            Debug.Log ("Fail to instantiate any enemy.");
            return;
        } else if (enemies.Count < spawnNumbers) {
            Debug.Log ("Instantiated " + enemies.Count + "enemies." );
        }
    }

    void Spawn () {
        if(spawnNumbers > spawnPoints.Count) {
            Debug.Log ("Spawn number is larger than spawn point number.");
            return;
        }

        for (int i = 0; i < spawnNumbers; i++) {
             // Find a random index between zero and one less than the number of spawn points.
            int spawnPointIndex = Random.Range (0, spawnPoints.Count);
            // check if that index has already been spawn
            while (isInstantiated[spawnPointIndex]) {
                spawnPointIndex = Random.Range (0, spawnPoints.Count);
            }
            isInstantiated[spawnPointIndex] = true;

            // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
            GameObject newEnemy = Instantiate (enemyPrefab, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation) as GameObject;
            // push the instantiated enemy into the array list if it exists
            if (newEnemy != null) {
                enemies.Add (newEnemy);
            } else {
                Debug.Log ("Fail to instantiate the enemy.");
                continue;
            }
        }        
    }
}