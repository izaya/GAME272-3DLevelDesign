﻿using UnityEngine;
using System.Collections;

public class Stairs : MonoBehaviour {
	GameObject blazier;

	void Start () {
		gameObject.SetActive (false);
		blazier = this.gameObject.transform.GetChild (0).gameObject;
		blazier.SetActive (false);
	}
	
	public void setVisibility (bool visibility) {
		gameObject.SetActive (visibility);
	}

	void OnTriggerEnter(Collider c) {
		if (c.tag == "Player") {
			blazier.SetActive (true);
		}
	}
}

