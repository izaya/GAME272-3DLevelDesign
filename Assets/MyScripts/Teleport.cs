﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {
	public AudioClip teleportSfx;   //Sound Effect used on teleport.

	// Use this for initialization
	void Start () {
		gameObject.SetActive (false);
	}
	
	void OnTriggerEnter(Collider c) {
		if (c.tag == "Player" && gameObject.activeSelf == true) {
			SoundManager.instance.Play(teleportSfx);
			c.gameObject.transform.Translate (Vector3.up * 34.0f);
		}
	}
}
