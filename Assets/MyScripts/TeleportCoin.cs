﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Class that extends Collectable which handles the coin interaction.
/// </summary>
public class TeleportCoin : Collectable {
	public GameObject fx;   //Particle Effect Prefab that is used on picked.
	public AudioClip teleportSfx;   //Sound Effect used on teleport.
	public Text CoinText;
	public GameObject teleport;	// teleport function transits to new object

	/// <summary>
	/// Overidden from Collectable when the object is touched by the player.
	/// </summary>
	/// <param name="player"></param>
	public override void Effect(GameObject player)
	{
		//If there is a specified FX spawn the particle effect.
		if (fx)
		{
			GameObject newFx = Instantiate(fx);
			newFx.transform.position = transform.position;
		}

		//If there is a UI element named CoinText Get the counter class and increase the value.
		if (CoinText) {
			CoinText.GetComponent<Counter>().Increase();
		} else if (GameObject.Find("CoinText")) {
			GameObject.Find("CoinText").GetComponent<Counter>().Increase();
		}

		SoundManager.instance.Play(teleportSfx);
		player.transform.Translate (Vector3.up * 34.0f);

		if (teleport) {
			teleport.SetActive (true);
		} else {
			Debug.Log ("Didn't link teleport to teleport coin");
		}

		//Destroy the collectable on pickup.
		Destroy(transform.parent.gameObject);
	}
}
