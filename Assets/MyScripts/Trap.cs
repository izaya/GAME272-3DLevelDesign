﻿using UnityEngine;
using System.Collections;

public class Trap : MonoBehaviour {
	public AudioClip scream;

	void OnTriggerEnter(Collider c) {
		if (c.tag == "Player") {
			gameObject.SetActive (false);

			if (scream) {
				SoundManager.instance.Play (scream);
			} else {
				Debug.Log ("Didn't link scream SFX to trap");
			}
		}
	}
}

