﻿using UnityEngine;
using System.Collections;

public class secret : MonoBehaviour {
	public AudioClip moan;
	public GameObject endpoint;

	void OnTriggerEnter(Collider c) {
		if (c.tag == "Player") {
			if (moan) {
				SoundManager.instance.Play (moan);
			} else {
				Debug.Log ("Didn't link oh-yeah SFX to secret Unity Chan");
			}

			if (endpoint) {
				c.gameObject.transform.position = endpoint.transform.position;
			} else {
				Debug.Log ("Didn't link teleport end point to secret Unity Chan");
			}

			Destroy(gameObject);
		}
	}
}

