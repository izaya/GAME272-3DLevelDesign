GAME272-3DLevelDesign
=====================
This is a school assignment of 3D level design. The avatar and castle assets are from Unity asset store and my professor [MJ](https://twitter.com/MichaelMJJohn) who's the former EA's creative director. 
I made the castle building from walls and some simple objects, health system, collectable system, collectable and enemy spawn manager, game manager, sound manager, minimap, some interactions, etc.  
The design idea is inspired by Super Mario 64 and the castle's inspiration is from Edinburgh Castle and Cathédrale Notre Dame de Paris.  
Check out [https://youtu.be/OuDdBg4Xwto](https://youtu.be/OuDdBg4Xwto) for gameplay demo video.  
<img src="/screenshot1.png">
<img src="/screenshot2.png">
### Project Structure
/Assets/level2.unity                                    -- The final level as you see in the demo  
/Assets/myScene.unity                               -- Level version 1  
/Assets/MyScripts                                       -- where all the customized scripts written by me locate  
/Assets/MyAssets                                        -- my prefabs, SFX and sprites
/Assets/3D Adventure Game Kit               -- avatar character and enemy provided by my professor MJ with prefabs, FBX, SFX, scripts in it  
/Assets/Medieval Castle Creation Kit        -- Castle assets purchased in Unity Asset Store  
/Assets/Medieval Bridge                             -- Bridge model imported from Unity Asset Store  
/Assets/Medieval house                              -- House models imported from Unity Asset Store
/Assets/UnityChan                                       -- UnityChan figure model imported from Unity Asset Store
/Assets/ProCore                                         -- Probuilder
### Gameplay
WASD to move forward, left, back and right.
Space key to jump.
Double jump is allowed.  
Jumping above the head of the goblins could damage and kill them.  
Collect coins to reach the end of the level.
### Credits
Avatar from [https://github.com/michael-mj-john/3DLevelDesign](https://github.com/michael-mj-john/3DLevelDesign)  
Castle assets are from Unity asset store [https://www.assetstore.unity3d.com/en/#!/content/27678](https://www.assetstore.unity3d.com/en/#!/content/27678)  
"Music: [www.bensound.com](http://www.bensound.com/)" or "[Royalty Free Music from Bensound](http://www.bensound.com/)"
